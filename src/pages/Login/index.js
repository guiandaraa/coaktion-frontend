import * as Yup from 'yup';
import { Formik } from 'formik';
import { Link, Link as RouterLink } from 'react-router-dom';
import {
    Box,
    Button,
    TextField,
    CircularProgress,
    Typography
} from '@mui/material';
import userServices from '../../services/userServices';
import { useContext } from 'react';
import { AuthContext } from '../../contexts/Auth';
import { useNavigate } from 'react-router-dom';

export default function Login() {
    const navigate = useNavigate();
    const { setUser } = useContext(AuthContext);

    return (
        <Box display='flex' justifyContent='center' alignItems='center' width='100%' height='100%'>
            <Box maxWidth='500px' width='80%' margin='auto' textAlign='center'>
                <Typography variant='h2'>
                    Entrar
                </Typography>
                <Formik
                    validateOnBlur={false}
                    initialValues={{
                        email: '',
                        password: '',
                        submit: null
                    }}

                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                            .email('Digite um email válido')
                            .max(255)
                            .required('O campo email é obrigatório'),
                        password: Yup.string()
                            .max(255)
                            .min(6, 'O campo senha deve conter ao menos 6 caracteres')
                            .required('O campo senha é obrigatório')
                    })}

                    onSubmit={async (values, { setErrors }) => {
                        try {
                            const user = await userServices.login(values);
                            await user
                            if (!user) {
                                setErrors({ password: 'Email e/ou senha incorreto(s)', email: 'Email e/ou senha incorreto(s)' });
                            }
                            else{
                                setUser(user);
                                navigate('/app/book-list')
                            }
                        } catch (err) {
                            console.log(err);
                            setErrors({ password: 'Ocorreu algum erro durante a tentativa de login' });
                        }
                    }}
                >
                    {({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting,
                        touched,
                        values
                    }) => (
                        <form noValidate onSubmit={handleSubmit}>

                            <TextField
                                error={(touched.email && errors.email)}
                                fullWidth
                                margin="normal"
                                autoFocus
                                helperText={touched.email && errors.email}
                                label={'Email'}
                                name="email"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                type="email"
                                value={values.email}
                                variant="outlined"
                            />
                            <TextField
                                error={(touched.password && errors.password)}
                                fullWidth
                                margin="normal"
                                helperText={touched.password && errors.password}
                                label={'Senha'}
                                name="password"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                type="password"
                                value={values.password}
                                variant="outlined"
                            />
                            <Box width='100%' textAlign='end'>
                                <Typography
                                    component="span"
                                    variant="subtitle3"
                                    color="text.secondary"
                                >
                                    {'Ainda não possui uma conta?'}
                                </Typography>
                                {' '}
                                <Link component={RouterLink} to="/register">
                                    <b>Cadastre-se aqui</b>
                                </Link>
                            </Box>
                            <Button
                                sx={{ mt: 3 }}
                                color="primary"
                                startIcon={isSubmitting ? <CircularProgress size="1rem" /> : null}
                                disabled={isSubmitting}
                                type="submit"
                                fullWidth
                                size="large"
                                variant="contained"
                            >
                                {'Entrar'}
                            </Button>
                        </form>
                    )}
                </Formik>
            </Box>
        </Box>
    );
}

