import * as Yup from 'yup';
import { Formik } from 'formik';
import { Link, Link as RouterLink } from 'react-router-dom';
import {
    Box,
    Button,
    TextField,
    CircularProgress,
    Typography,
    Checkbox
} from '@mui/material';
import userServices from '../../services/userServices';
import { useContext } from 'react';
import { AuthContext } from '../../contexts/Auth';
import { useNavigate } from 'react-router-dom';


export default function Register() {
    const navigate = useNavigate();
    const { setUser } = useContext(AuthContext);

    return (
        <Box display='flex' justifyContent='center' alignItems='center' width='100%' height='100%' margin='5px'>
            <Box maxWidth='500px' width='80%' margin='auto' textAlign='center' >
                <Typography variant='h2'>
                    Cadastrar
                </Typography>
                <Formik
                    validateOnBlur={false}
                    initialValues={{
                        name: '',
                        email: '',
                        password: '',
                        role: 'user',
                        submit: null
                    }}

                    validationSchema={Yup.object().shape({
                        name: Yup.string()
                            .max(255)
                            .required('O campo nome é obrigatório'),
                        email: Yup.string()
                            .email('Digite um email válido')
                            .max(255)
                            .required('O campo email é obrigatório'),
                        password: Yup.string()
                            .max(255)
                            .min(6, 'O campo senha deve conter ao menos 6 caracteres')
                            .required('O campo senha é obrigatório'),

                    })}

                    onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                        try {
                            const user = await userServices.createUser(values);
                            if (user) {
                                setUser(user);
                                navigate('/app/book-list')
                            }

                        } catch (err) {
                            console.log(err);
                            setErrors({ password: 'Ocorreu algum erro durante a criação do usuário' });
                        }
                    }}
                >
                    {({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting,
                        touched,
                        values
                    }) => (
                        <form noValidate onSubmit={handleSubmit}>
                            <TextField
                                error={(touched.name && errors.name)}
                                fullWidth
                                autoFocus
                                margin="normal"
                                helperText={touched.name && errors.name}
                                label={'Nome'}
                                name="name"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.name}
                                variant="outlined"
                            />
                            <TextField
                                error={(touched.email && errors.email)}
                                fullWidth
                                margin="normal"
                                helperText={touched.email && errors.email}
                                label={'Email'}
                                name="email"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                type="email"
                                value={values.email}
                                variant="outlined"
                            />
                            <TextField
                                error={(touched.password && errors.password)}
                                fullWidth
                                margin="normal"
                                helperText={touched.password && errors.password}
                                label={'Senha'}
                                name="password"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                type="password"
                                value={values.password}
                                variant="outlined"
                            />
                            <Box width='100%' display='flex' justifyContent='space-between'>
                                <Box>
                                    <Checkbox
                                        color="primary"
                                        onChange={(event) => {
                                            event.target.checked ?
                                                values.role = 'admin'
                                                :
                                                values.role = 'user'
                                        }}
                                    />
                                    <Typography
                                        component="span"
                                        variant="subtitle3"
                                        color="text.secondary"
                                        fontWeight="bold"
                                    >
                                        {'Admin?'}
                                    </Typography>
                                </Box>
                                <Box mt={1}>

                                    <Typography
                                        component="span"
                                        variant="subtitle3"
                                        color="text.secondary"
                                    >
                                        {'Já possui uma conta?'}
                                    </Typography>
                                    <Link component={RouterLink} to="/login">
                                        {' '}<b>Entre aqui</b>
                                    </Link>
                                </Box>
                            </Box>
                            <Button
                                sx={{ mt: 3 }}
                                color="primary"
                                startIcon={isSubmitting ? <CircularProgress size="1rem" /> : null}
                                disabled={isSubmitting}
                                type="submit"
                                fullWidth
                                size="large"
                                variant="contained"
                            >
                                {'Cadastrar'}
                            </Button>
                        </form>
                    )}
                </Formik>
            </Box>
        </Box >
    );
}

