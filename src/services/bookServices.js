import axios from 'axios'

const client = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL
})

const userServices = {
    async execute(method, resource, data) {
        return client({
            method,
            url: resource,
            data,
        }).then(req => {
            return req.data
        }).catch(err => {
            return err;
        })
    },

    createBook(book) {
        return this.execute('post', `book`, book);
    },
    getBookById(id) {
        return this.execute('get', `book/${id}`);
    },
    editBook(book, id) {
        return this.execute('put', `book/${id}`, book);
    },
    getAllBooks() {
        return this.execute('get', `book`);
    },
    deleteBook(id) {
        return this.execute('delete', `book/${id}`);
    }
}

export default userServices;