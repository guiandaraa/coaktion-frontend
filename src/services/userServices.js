import axios from 'axios'

const client = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL
})

const userServices = {
    async execute(method, resource, data) {
        return client({
            method,
            url: resource,
            data,
        }).then(req => {
            return req.data
        }).catch(err => {
            return err;
        })
    },

    login(user) {
        return this.execute('post', `user/login`, user);
    },
    createUser(user) {
        return this.execute('post', `user`, user);
    },
    deleteUser(id) {
        return this.execute('delete', `user/${id}`);
    },
}

export default userServices;