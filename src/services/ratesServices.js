import axios from 'axios'

const client = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL
})

const userServices = {
    async execute(method, resource, data) {
        return client({
            method,
            url: resource,
            data,
        }).then(req => {
            return req.data
        }).catch(err => {
            return err;
        })
    },

    getRatesByUserId(id) {
        return this.execute('get', `rate/user/${id}`);
    },
    getRatesByBookId(id) {
        return this.execute('get', `rate/book/${id}`);
    },
    createRate(rate) {
        return this.execute('post', `rate`, rate);
    }
}

export default userServices;