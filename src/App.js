import './App.css';
import Login from './pages/Login';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Register from './pages/Register';
import BookCreateForm from './containers/BookCreateForm';
import BookEditForm from './containers/BookEditForm';
import BookList from './containers/BookList';
import LayoutContext from './contexts/Layout';
import { AuthProvider } from './contexts/Auth';

function App() {
  return (
    <div style={{ width: '100vw', height: '100vh' }}>
      < BrowserRouter >
        <AuthProvider>
          <LayoutContext >
            <Routes>
              <Route path={'/Register'} element={<Register />} />
              <Route path={'/Login'} element={<Login />} />
              <Route path={'/app/create-book'} element={<BookCreateForm />} />
              <Route path={'/app/edit-book/:id'} element={<BookEditForm />} />
              <Route path={'/app/book-list'} element={<BookList />} />
              <Route path={'/*'} element={<Login />} />
            </Routes >
          </LayoutContext>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
