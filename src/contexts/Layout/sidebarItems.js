import MenuBookIcon from '@mui/icons-material/MenuBook';
import BookmarkAddIcon from '@mui/icons-material/BookmarkAdd';

const sidebarItems = [
    {
        name: 'Livros',
        icon: <MenuBookIcon />,
        route: '/app/book-list',
        role: 'user'
    },
    {
        name: 'Cadastrar  Livros',
        icon: <BookmarkAddIcon />,
        route: '/app/create-book',
        role: 'admin'
    }
]

export default sidebarItems;