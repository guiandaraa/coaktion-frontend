import { createContext, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";

export const AuthContext = createContext({});


export const AuthProvider = (props) => {

    const navigate = useNavigate();
    const { pathname } = useLocation();
    const [user, setUser] = useState({});



    useEffect(() => {
        if (!user.name && pathname.includes('/app')) {
            navigate('/login');
        }
    }, [pathname])

    return (
        <AuthContext.Provider value={{ user, setUser }} >
            {props.children}
        </AuthContext.Provider>
    );
}