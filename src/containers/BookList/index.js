import { Button, Table, TableBody, TableCell, TableHead, TableRow, Typography, Box, TableContainer, Dialog } from "@mui/material";
import { useEffect, useState } from "react";
import bookServices from '../../services/bookServices';
import Paper from '@mui/material/Paper';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { Link } from 'react-router-dom';
import CommentIcon from '@mui/icons-material/Comment';
import ratesService from '../../services/ratesServices';
import RateCreateForm from '../RateCreateForm';
import RateListByBook from '../RateListByBook';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import { useContext } from 'react';
import { AuthContext } from '../../contexts/Auth';


export default function BookList() {

    const [currentBooks, setCurrentBooks] = useState([]);
    const [ratedBooks, setRatedBooks] = useState([]);
    const [loading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(true);
    const [openRateForm, setOpenRateForm] = useState(false);
    const [openBookRates, setOpenBookRates] = useState(false);
    const [bookToHandle, setBookToHandle] = useState('');
    const { user } = useContext(AuthContext);


    const openRateFormHandler = async (book_id) => {
        setBookToHandle(book_id)
        setOpenRateForm(true);
    }

    const closeRateFormHandler = async () => {
        setOpenRateForm(false);
    }
    const closeRateListHandler = async () => {
        setOpenBookRates(false);
    }

    const openBookRatesHandler = async (book_id) => {
        setBookToHandle(book_id);
        setOpenBookRates(true);
    }

    const deleteBookHandler = async (book_id) => {
        try{
            await bookServices.deleteBook(book_id)
        }catch(err){
            console.log(err);
        }
        setRefresh(true);
    }

    useEffect(() => {
        if (refresh) {
            const getBooks = async () => {

                const books = await bookServices.getAllBooks();

                setCurrentBooks(books);
                if (books.length > 0) {
                    setLoading(false);
                } else {
                    alert('nenhum livro cadastrado')
                }
            }

            const getUserRates = async () => {
                const rates = await ratesService.getRatesByUserId(user._id);
                setRatedBooks(rates);
            }
            getBooks();
            getUserRates();
            setRefresh(false);
        }
    }, [refresh])

    if (loading) {
        return (
            <>
            </>
        )
    }

    return (
        <Box width='80%' minWidth={450} >
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table" margin='auto'>
                    <TableHead >
                        <TableRow>
                            <TableCell>
                                <Typography fontWeight='bold'>
                                    Titulo
                                </Typography>
                            </TableCell>
                            <TableCell >
                                <Typography fontWeight='bold'>
                                    Autor
                                </Typography>
                            </TableCell>
                            <TableCell align="center">
                                <Typography fontWeight='bold'>
                                    Descrição
                                </Typography>
                            </TableCell >
                            <TableCell align="right" />

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {currentBooks.map((book) => (
                            <TableRow
                                key={book._id}
                            >
                                <TableCell component="th" scope="row">
                                    {book.title}
                                </TableCell>
                                <TableCell>{book.author}</TableCell>
                                <TableCell align="center">{book.description}</TableCell>
                                <TableCell align="right">
                                    {
                                        user.role === 'admin' ?
                                            <Box p={1}>
                                                <Link to={`/app/edit-book/${book._id}`}>
                                                    <Button>
                                                        <EditIcon />
                                                    </Button>
                                                </Link>
                                                <Button onClick={() => deleteBookHandler(book._id)}>
                                                    <DeleteIcon />
                                                </Button>

                                            </Box>
                                            :
                                            <Box p={1}>
                                                <Button
                                                    disabled={ratedBooks.some((rate) => rate.book_id === book._id)}
                                                    onClick={() => openRateFormHandler(book._id)}
                                                >
                                                    <CommentIcon />
                                                </Button>
                                                <Button
                                                    onClick={() => openBookRatesHandler(book._id)}
                                                >
                                                    <QuestionAnswerIcon />
                                                </Button>
                                            </Box>
                                    }

                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Dialog open={openRateForm} onClose={closeRateFormHandler}>
                <RateCreateForm book_id={bookToHandle} setOpen={setOpenRateForm} setRefresh={setRefresh} />
            </Dialog>
            <Dialog open={openBookRates} onClose={closeRateListHandler}>
                <RateListByBook book_id={bookToHandle} setOpen={setOpenBookRates} />
            </Dialog>

        </Box>
    );
}