import * as Yup from 'yup';
import { Formik } from 'formik';
import {
    Box,
    Button,
    TextField,
    CircularProgress,
    Typography,
    Rating,
    Divider
} from '@mui/material';
import rateServices from '../../services/ratesServices';
import { useContext } from 'react';
import { AuthContext } from '../../contexts/Auth';

export default function RateCreateForm({ book_id, setOpen, setRefresh }) {

    const { user  } = useContext(AuthContext);

    return (
        <Box margin='auto' textAlign='center' p={4}>
            <Typography variant='h2'>
                Avaliação
            </Typography>
            <Divider />

            <Formik
                validateOnBlur={false}
                initialValues={{
                    rate: 0,
                    comment: '',
                    submit: null
                }}

                validationSchema={Yup.object().shape({
                    rate: Yup.number()
                        .required('O campo estrela é obrigatório'),
                    comment: Yup.string()
                        .max(250)
                        .required('O campo descrição é obrigatório')
                })}

                onSubmit={async (values) => {
                    try {
                        values.user_id = user._id;
                        values.book_id = book_id;
                        await rateServices.createRate(values);
                        setOpen(false);
                        setRefresh(true);
                    } catch (err) {
                        console.log(err);
                    }
                }}
            >
                {({
                    errors,
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    isSubmitting,
                    touched,
                    values
                }) => (
                    <form noValidate onSubmit={handleSubmit}>
                        <Box mt={4}>
                            <Rating size='large'
                                error={(touched.rate && errors.rate)}
                                helperText={touched.rate && errors.rate}
                                name='rate'
                                value={values.points}
                                onChange={handleChange}
                            />
                        </Box>
                        <TextField
                            error={(touched.comment && errors.comment)}
                            helperText={touched.comment && errors.comment}
                            fullWidth
                            margin="normal"
                            label={'Descrição'}
                            name="comment"
                            multiline
                            rows={4}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.comment}
                            variant="outlined"
                        />
                        <Button
                            sx={{ mt: 3 }}
                            color="primary"
                            startIcon={isSubmitting ? <CircularProgress size="1rem" /> : null}
                            disabled={isSubmitting}
                            type="submit"
                            fullWidth
                            size="large"
                            variant="contained"
                        >
                            {'Enviar avaliação'}
                        </Button>
                    </form>
                )}
            </Formik>
        </Box>
    );
}

