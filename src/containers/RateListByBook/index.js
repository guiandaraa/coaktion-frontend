import { Table, TableBody, TableCell, TableHead, TableRow, Typography, Box, TableContainer } from "@mui/material";
import { useEffect, useState } from "react";
import Paper from '@mui/material/Paper';
import rateServices from '../../services/ratesServices';


export default function BookList({ book_id, setOpen }) {

    const [loading, setLoading] = useState(true);
    const [refresh, setRefresh] = useState(true);
    const [bookToShowRates, setBookToShowRates] = useState('');

    useEffect(() => {
        if (refresh) {
            const getBooks = async () => {
                const rates = await rateServices.getRatesByBookId(book_id);
                if (rates.length === 0) {
                    alert('nenhuma avaliação cadastrada');
                    setOpen(false);
                }
                setBookToShowRates(rates);
                setLoading(false);
            }
            getBooks();
            setRefresh(false);
        }
    }, [refresh])

    if (loading) {
        return (
            <>
            </>
        )
    }

    return (
        <Box margin='auto' textAlign='center' p={4}>
            <TableContainer component={Paper}>
                <Typography variant='h6' m={2}>
                    Avaliações do livro
                </Typography>
                <Table sx={{ minWidth: 350 }} aria-label="simple table" margin='auto'>
                    <TableHead >
                        <TableRow>
                            <TableCell>
                                <Typography fontWeight='bold'>
                                    Nota
                                </Typography>
                            </TableCell>
                            <TableCell align="center">
                                <Typography fontWeight='bold'>
                                    Comentário
                                </Typography>
                            </TableCell >

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {bookToShowRates.map((rating) => (
                            <TableRow
                                key={rating._id}
                            >
                                <TableCell component="th" scope="row">
                                    {rating.rate}
                                </TableCell>
                                <TableCell align="center">{rating.comment}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    );
}