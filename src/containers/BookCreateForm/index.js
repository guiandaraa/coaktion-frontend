import * as Yup from 'yup';
import { Formik } from 'formik';
import {
    Box,
    Button,
    TextField,
    CircularProgress,
    Typography
} from '@mui/material';
import bookServices from '../../services/bookServices';
import { useNavigate } from 'react-router';

export default function BookCreateForm() {

    const navigate = useNavigate();

    return (
        <Box display='flex' justifyContent='center' alignItems='center' width='100%' height='100%'>
            <Box maxWidth='500px' width='80%' margin='auto' textAlign='center'>
                <Typography variant='h2'>
                    Cadastrar Livro
                </Typography>
                <Formik
                    validateOnBlur={false}
                    initialValues={{
                        title: '',
                        author: '',
                        description: '',
                        submit: null
                    }}

                    validationSchema={Yup.object().shape({
                        title: Yup.string()
                            .max(100)
                            .required('O campo título é obrigatório'),
                        author: Yup.string()
                            .max(100)
                            .required('O campo author é obrigatório'),
                        description: Yup.string()
                            .max(250)
                            .required('O campo descrição é obrigatório')
                    })}

                    onSubmit={async (values, { setErrors }) => {
                        try {
                             await bookServices.createBook(values);
                             navigate('/app/book-list')
                        } catch (err) {
                            console.log(err);
                            setErrors({ description: 'Ocorreu algum erro durante a criação do livro'});
                        }
                    }}
                >
                    {({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting,
                        touched,
                        values
                    }) => (
                        <form noValidate onSubmit={handleSubmit}>

                            <TextField
                                error={(touched.title && errors.title)}
                                fullWidth
                                margin="normal"
                                autoFocus
                                helperText={touched.title && errors.title}
                                label={'Título'}
                                name="title"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.title}
                                variant="outlined"
                            />
                            <TextField
                                error={(touched.author && errors.author)}
                                fullWidth
                                margin="normal"
                                helperText={touched.author && errors.author}
                                label={'Autor'}
                                name="author"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.author}
                                variant="outlined"
                            />
                            <TextField
                                error={(touched.description && errors.description)}
                                fullWidth
                                margin="normal"
                                helperText={touched.description && errors.description}
                                multiline
                                rows={4}
                                label={'Descrição'}
                                name="description"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                value={values.description}
                                variant="outlined"
                            />
                            <Button
                                sx={{ mt: 3 }}
                                color="primary"
                                startIcon={isSubmitting ? <CircularProgress size="1rem" /> : null}
                                disabled={isSubmitting}
                                type="submit"
                                fullWidth
                                size="large"
                                variant="contained"
                            >
                                {'Cadastrar Livro'}
                            </Button>
                        </form>
                    )}
                </Formik>
            </Box>
        </Box>
    );
}

